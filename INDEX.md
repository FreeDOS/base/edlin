# EDLIN

Active development at [https://sourceforge.net/projects/freedos-edlin/](https://sourceforge.net/projects/freedos-edlin/)
# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## EDLIN.LSM

<table>
<tr><td>title</td><td>EDLIN</td></tr>
<tr><td>version</td><td>2.24</td></tr>
<tr><td>entered&nbsp;date</td><td>2024-05-16</td></tr>
<tr><td>description</td><td>The FreeDOS standard line editor</td></tr>
<tr><td>summary</td><td>The edlin program is the FreeDOS standard line editor. (UPX Compressed)</td></tr>
<tr><td>keywords</td><td>edit, editor, line editor</td></tr>
<tr><td>author</td><td>Gregory Pietsch &lt;gpietsch -at- comcast.net&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Gregory Pietsch &lt;gpietsch -at- users.sourceforge.net&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>http://sourceforge.net/projects/freedos-edlin</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/edlin</td></tr>
<tr><td>original&nbsp;site</td><td>http://sourceforge.net/projects/freedos-edlin</td></tr>
<tr><td>platforms</td><td>DOS (Microsoft Visual C++ in C mode, Borland C++), FreeDOS, OpenWatcom, Linux (gcc)</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Edlin</td></tr>
</table>
